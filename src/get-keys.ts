import { Properties, getProperties } from 'properties-file';
import { PropertiesEditor } from 'properties-file/editor';

const args = process.argv.slice(2);

if (args[0] == '' || args[0] == undefined) throw new Error('No file specified');
const file = Bun.file(args[0]);

let parsed = getProperties(await file.text());
console.log(JSON.stringify(Object.keys(parsed)));