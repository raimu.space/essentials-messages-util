# essentials-messages-util

This is a tool that will automatically **add a prefix to a messages_xx.properties file for EssentialsX**, while *not adding it to messages that shouldn't have it*.

If you find a mistake (a message that's categorized incorrectly, or maybe you get a "Unknown key" error), please make an issue in [the repo](https://gitlab.com/raimu.space/essentials-messages-util) so it can be updated!

## Usage

To install dependencies:

```bash
bun install
```

View help:

```bash
bun start
```